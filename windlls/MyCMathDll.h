#ifdef MYCMATHDLL_EXPORTS
#define MYCMATHDLL_API __declspec(dllexport) 
#else
#define MYCMATHDLL_API __declspec(dllimport) 
#endif

#define PI 3.1415

#ifdef __cplusplus
extern "C" {
#endif

double MYCMATHDLL_API  PowerOf2 (double UserNumber);
MYCMATHDLL_API double PowerOf3 (double UserNumber);
MYCMATHDLL_API double CircleArea (double UserRadius);
MYCMATHDLL_API double CircleCircum (double UserRadius);

#ifdef __cplusplus
}
#endif
